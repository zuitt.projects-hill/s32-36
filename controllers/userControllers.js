// import bcrypt module
const bcrypt = require("bcrypt");

// import User model
const User = require("../models/User");

const Course = require("../models/Course")

// import auth module
const auth = require("../auth");

// Controllers

// User Registration

module.exports.registerUser = (req, res) => {

	console.log(req.body);

	// bcrypt - adds a layer of security to your user's password

	// what bcrypt does is hash our password into a randomized character version of the origional string

	// Syntax: brcypt.hashSynce(<string to be hashed>, <saltRounds>)

	// Salt-Rounds are the number of times the characters in the hash are randomized

	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	// Create a new user document out of our user model

	let newUser = new User({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	})

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error));
};

// Retrieve All Users

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

//Login

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	/*
		1. find by user email
		2. if a user email is found, check the password
		3. if we don't find the user email, send a message
		4. if upon checking the found users password is the same as our input password, we will generate a "key" to access our app. If not, we will turn him away by sending a message to the client

	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){

			return res.send("User does not exist");

		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if(isPasswordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send("Password is incorrect.")
			}
		}
	})
	.catch(err => res.send(err));
};

// Get User Details

module.exports.getUserDetails = (req, res) => {

	console.log(req.user)

	// 1. find a logged in users document from our database and send it to the client by its ID

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.checkEmailExists = (req, res) => {

	User.findOne({email: req.body.email})
		.then(foundEmail => {

			if(foundEmail === null){

				return res.send("This Email is Available");

			} else {

					return res.send("This Email is already registered.")
				}
			})
		.catch(error => res.send(error));
	
}

// Updating a regular user to Admin

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id);
	console.log(req.params.id);

	let updates = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};

// Update User Details
module.exports.updateUserDetails = (req, res) => {

	console.log(req.body); // checking the input for new values for our users details
	console.log(req.user.id); //check the logged in user

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));
};

// Enroll a user

module.exports.enroll = async (req, res) => {

	/*
		Enrollment Process:

		1. look for the user by id
			- push the details of the course we are trying to enroll in.
				- we'll push to a new enrollment subdocument in our user

		2. Look for the course by it's id
			- push the details of the enrollees/user whos trying to enroll
				- we'll push to a new enrollee subdocument in our course

		3. When both saving documents are successful, we send a message to the client
	*/

	console.log(req.user.id);
	console.log(req.body.courseId);

	// checking if a user is admin, if he is he should not be able to enroll

	if(req.user.isAdmin){
		return res.send("Action Forbidden");
	}

	/*
		Find the user:

		async - a keyword that allows us to make our function asynchronous. Which means, that instead of JS regular behavior of each code line by line, it will allow us to wait for the result of the function.

		await - a keyword that allows us to wait for the function to finish before proceeding 

	*/
	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

		console.log(user);

		// add the courseId in an object and push that object into user's enrollment array
		let newEnrollment = {
			courseId: req.body.courseId
		}

		user.enrollments.push(newEnrollment);

		return user.save()
		.then(user => true)
		.catch(error => error.message);
	})

	// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with our message.

	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}

	let isCourseUpdated = await Course.findById(req.body.courseId)
	.then(course => {

		console.log(course);

		// create an object which will contain the user id of the enrollee of a course
		let enrollee = {
			userId: req.user.id
		}

		course.enrollees.push(enrollee);

		return course.save()
		.then(course => true)
		.catch(error => error.message);

	})

	if(isCourseUpdated !== true){
		return res.send({message: isCourseUpdated})
	}

	if(isUserUpdated && isCourseUpdated){
		return res.send({message: 'Enrolled Successfully!'})
	}


};

module.exports.getEnrollments = (req, res) => {

	User.findById(req.user.id)
	.then(result => {res.send(result.enrollments)})
	.catch(error => res.send(error));
};