const Course = require("../models/Course");

module.exports.addCourse = (req, res) => {

	console.log(req.body);

	let newCourse = new Course({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error));
};

// Retrieve All Courses

module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};


module.exports.getSingleCourse = (req, res) => {

	console.log(req.params);

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

module.exports.archiveCourse = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));
};

module.exports.activateCourse = (req, res) => {

	console.log(req.params.id);

	let updates = {
		isActive: true
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));
};

module.exports.getActiveCourses = (req, res) => {

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.updateCourse = (req, res) => {

	console.log(req.params.id);
	console.log(req.body);

	let update = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, update, {new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));
};

module.exports.getInactiveCourses = (req, res) => {

	Course.find({isActive:false})
	.then(result => res.send(result))
	.catch(error => res.send(error));
};

module.exports.findCoursesByName = (req, res) => {

	Course.find({name: {$regex: req.body.name, $options:`$i`}})
	.then(result => {
		if(result.length === 0){
			return res.send("No courses found");
		} else {
			return res.send(result);
		}
	})
	.catch(error => res.send(error));
};

module.exports.findCoursesByPrice = (req, res) => {

	Course.find({price: req.body.price})
	.then(result => {

		if(result.length === 0){
			return res.send("No courses found");

		} else {
			return res.send(result);
		}
	})
	.catch(error => res.send(error));
	};

module.exports.getEnrollees = (req, res) => {

	Course.findById(req.params.id)
	.then(result => {res.send(result.enrollees)})
	.catch(error => res.send(error));
};