const express = require("express");
const router = express.Router();
//import course controllers
const courseControllers = require("../controllers/courseControllers");

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

// routes

// Add Course
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// Get all Courses
router.get('/', courseControllers.getAllCourses);

router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

router.put("/archive/:id", verify, verifyAdmin, courseControllers.archiveCourse);

router.put("/activate/:id", verify, verifyAdmin, courseControllers.activateCourse);

router.get("/getActiveCourses", courseControllers.getActiveCourses);

//update a course
router.put("/:id", verify, verifyAdmin, courseControllers.updateCourse);

// inactive courses
router.get("/getInactiveCourses", verify, verifyAdmin, courseControllers.getInactiveCourses);

// find courses by name
router.post("/findCoursesByName", courseControllers.findCoursesByName);

// find courses by price
router.post("/findCoursesByPrice", courseControllers.findCoursesByPrice);

router.get("/getEnrollees/:id", verify, verifyAdmin, courseControllers.getEnrollees)

module.exports = router;
